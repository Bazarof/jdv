#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>

#define TAM 20

int inicializa_tablero();
int despliega_tablero();
int control_manual();
int generacion();
int nvecinos(int i, int j);

int tablero[TAM][TAM];

int main(int argc, char *argv[])
{
	char opc; // variable de opcion
	inicializa_tablero();
	do
	{ // Hacer
		despliega_tablero();
		printf("\nPresione una tecla para continuar, <esc> para cancelar...");
		printf("\nPresione la tecla \'C\' para ingresar una celula de forma manual...");
		opc = getch();
		if((opc == 'C')||(opc == 'c')) control_manual();
		if(opc != 27)generacion();
		
	} while (opc != 27); // Mientras la opcion sea diferente de ESC
	return 0;
}

int inicializa_tablero()
{
	int i, j;
	srand(time(NULL));
	for (i = 0; i < TAM; i++)
	{
		for (j = 0; j < TAM; j++)
		{
			tablero[i][j] = rand() % 2;
		}
	}
	return 0;
}

int despliega_tablero()
{
	int i, j; // Variables iteradoras
	system("cls");
	printf("Bienvenido al juego de la vida de John Conway implementado en Lenguaje C\n\n");
	for (i = 0; i < TAM; i++)
	{
		for (j = 0; j < TAM; j++)
		{
			if (tablero[i][j])
			{					   // Si en la posicion fila i y columna j se encuentra un 1
				printf(" %c ", 1); // Imprime 1
			}
			else
			{
				printf(" . "); // Imprime un punto
			}
		}
		printf("\n");
	}
	return 0;
}

int control_manual()
{
	int i, j;
	char opc;
	do{
		printf("\n[Fila] [Columna]: ");
		scanf("%i",&i);
		scanf("%i",&j);

		if(tablero[i][j] == 1)	tablero[i][j] = 0;
		else tablero[i][j] = 1;

		despliega_tablero();

		printf("Desea insertar otra celula en el tablero? (S/N)");
		opc = getch();
	}while((opc != 'N')&&(opc != 'n'));
	return 0;
}

int generacion()
{
	int i, j, vecinos;
	int ntablero[TAM][TAM]; // tablero copia
	// Se inicializa ntablero a cero
	for (i = 0; i < TAM; i++)
	{
		for (j = 0; j < TAM; j++)
		{
			ntablero[i][j] = 0;
		}
	}
	// Se determinan las celulas que viviran en ntablero
	for (i = 0; i < TAM; i++)
	{
		for (j = 0; j < TAM; j++)
		{
			vecinos = nvecinos(i, j); // Se revisa los vecinos de cada posicicion de la matriz
			if ((tablero[i][j] == 1) && (vecinos == 2 || vecinos == 3))
			{
				ntablero[i][j] = 1;
			}
			if ((tablero[i][j] == 0) && (vecinos == 3))
			{
				ntablero[i][j] = 1;
			}
		}
	}
	for (i = 0; i < TAM; i++)
	{
		for (j = 0; j < TAM; j++)
		{
			tablero[i][j] = ntablero[i][j];
		}
	}
	return 0;
}

int nvecinos(int i, int j)
{
	int ren, col, n = 0;
	for (ren = i - 1; ren <= i + 1; ren++)
	{
		for (col = j - 1; col <= j + 1; col++)
		{
			if (!(ren == i && col == j) && (ren >= 0 && col >= 0) && (ren < TAM && col < TAM))
			{
				if (tablero[ren][col] == 1)
				{
					n++;
				}
			}
		}
	}
	return n;
}
